## Public MMA OpenFOAM Repository ##
### Mathematical Modeling and Analysis Group, TU Darmstadt ###

This repository consists of several codes which 

* implement the main representatives of two-phase Direct Numerical Simulation (DNS) methods, or 
* extend two-phase DNS methods to cope with additional transport processes at/on fluid interfaces

on the basis of a unified C++ development plattform, the Open Source CFD library OpenFOAM. Code and method development has been done in the junior research group **Advanced Two-Phase and Interfacial Flow Simulations using OpenFOAM** ([Dr.-Ing. Holger Marschall](mailto:marschall@mma.tu-darmstadt.de)) in the [Mathematical Modeling and Analysis Group](http://www.mma.tu-darmstadt.de) ([Prof. Dr. rer. nat. Dieter Bothe](mailto:bothe@mma.tu-darmstadt.de)) at Technische Universit&auml;t Darmstadt.

![Alt text](https://www.holger-marschall.owncube.com/public.php?service=files&t=bf80181e4ff35a05d5769dcd89b2ad6e&download "method portfolio for direct numerical simulations of two-phase flows")

In particular, this repository encompasses (2018-03-01) 

* [_interCSTFoam_](https://bitbucket.org/cstmethod) (CST, Continuous Species Transfer) : interfacial heat an mass transfer solver for the algebraic Volume-Of-Fluid method implemented in interFoam, main developers: [Daniel Deising](mailto:deising@mma.tu-darmstadt.de), [Simon Hill](mailto:simon.hill@tum.de)

and is to be further extended as we proceed with our research and development.